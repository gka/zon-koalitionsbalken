# coalition stacked bar chart

$ () ->

    _coalitions = ["CDU,SPD", "CDU,FDP", "CDU,FDP,PIRATEN", "CDU,GRÜNE", "SPD,GRÜNE", "SPD,FDP",
        "SPD,LINKE", "CDU,FDP,GRÜNE", "SPD,FDP,GRÜNE", "SPD,LINKE,GRÜNE"]
    _cont = $ '#canvas'
    _lblcont = $ '.vis-coalitions'
    canvas = Raphael _cont.get 0
    bar_w = 30
    elections = null
    width = _cont.width()
    height = _cont.height()-12
    stacked_bars = {}
    current_poll = null
    grid = {}
    bg = null
    partyColors = Common.partyColors
    winner =
        #'13': 'SPD,GRÜNE'
        '08': 'CDU,FDP'
        '03': 'CDU,FDP'
        '98': 'SPD'
        '94': 'SPD'
        '90': 'SPD,GRÜNE'

    HESSEN = $('.vis-coalitions').data('url') == 'hessen.json'

    if not HESSEN
        _coalitions.push "CDU,LINKE"

    $.fn.set = (txt) ->
        $('span', this).html txt

    get_coalitions = (poll, justParties) ->
        seats = poll.seats
        coalitions = []
        sum_seats = _.reduce seats, (s, v) ->
            s + v
        , 0

        min_seats = Math.ceil((sum_seats + 0.5) * 0.5)

        _.each _coalitions, (parties, i) ->
            coalition =
                id: i
                key: parties
                parties: []
                seats: 0

            _.each parties.split(','), (party) ->
                if not coalition
                    return
                if seats[party] # if party has results
                    coalition.parties.push  # add it to coalition
                        name: party
                        seats: seats[party]
                    # and add their seats to the coalition seats
                    coalition.seats += seats[party]
                else
                    coalition = null
                    return false

            if coalition?
                # sort parties in coalition by seats
                coalition.parties.sort (a,b) ->
                    b.seats - a.seats
                coalitions.push coalition

        # filter coalitions that make no sense
        coalitions = coalitions.filter (coalition) ->
            if coalition.parties.length == 3
                makessense = true
                _.each coalitions, (c) ->
                    if c.parties.length == 2
                        if c.parties[0].name == coalition.parties[0].name
                            if c.parties[1].name == coalition.parties[1].name or c.parties[1].name == coalition.parties[2].name
                                if c.seats >= poll.min_seats
                                    makessense = false
                                    return false
                makessense
            else
                true

        # add individual parties for comparison
        _.each seats, (s, party) ->
            if s > min_seats * 0.5 or (justParties and s > 0)
                coalitions.push
                    key: party
                    seats: s
                    parties: [{ name: party, seats: s }]

        # sort coalitions by majority
        coalitions.sort (a,b) ->
            if a.seats >= min_seats && b.seats >= min_seats
                # sort by biggest party first, bc they have the
                # privilege to set a government
                if a.parties[0].seats != b.parties[0].seats
                    return b.parties[0].seats - a.parties[0].seats
            b.seats - a.seats;
        # return coalitions
        poll.min_seats = min_seats
        coalitions

    longInstName =
        ARD: 'ARD / Infratest dimap'
        ZDF: 'ZDF / Forschungsgr. Wahlen'
        RTL: 'RTL / Forsa'
        Wahlrecht: 'Wahlrecht.de'
        'Vorläufiges<br>Endergebnis': 'Vorläufiges Endergebnis'

    render = (poll, justParties=false, firstLoad=false) ->

        END = poll.time == ''
        $dp = $('.desc-possible')
        if END and $dp.html() != $dp.data('end')
            $dp.data('hochrechnung', $dp.html())
            $dp.html($dp.data('end'))
        else if not END and $dp.data('hochrechnung')
            $dp.html($dp.data('hochrechnung'))

        if END
            $('.info').addClass('end')
        else
            $('.info').removeClass('end')

        $('.info').html ''+longInstName[poll.institute]+(if poll.time then ', Stand: '+poll.time else '')

        coalitions = get_coalitions poll, justParties
        bar_w = if justParties then 60 else 30

        yscale = (seats) ->
            # scale according to biggest coalition seats
            Math.round seats / coalitions[0].seats * height

        label = (clss, txt) ->
            lbl = $ '<div class="label '+clss+'"><span /></div>'
            _lblcont.append lbl
            lbl.css
                opacity: 0
            if txt?
                lbl.set txt
            lbl

        $('.desc').hide()
        setTimeout () ->
            $('.desc').fadeIn 400
        , 1000

        # $('.label.top').animate
        #     opacity: 0

        bar_w = Math.round((width - 220) / coalitions.length / 1.8)
        offset = width - coalitions.length * bar_w * 1.8 - 15

        if justParties
            bar_w = (width - 340) / coalitions.length / 1.8

        # animate bars for each coalition
        possible = true
        $.each coalitions, (i, coalition) ->
            if not stacked_bars[coalition.key]?
                # first occurance of this coalition, create bars
                sbc = stacked_bars[coalition.key] = {}
                $.each coalition.parties, (j, party) ->
                    x = 20 + i * bar_w * 1.8
                    x += 340 if justParties
                    y = 0
                    h = 0
                    bar = canvas.rect x, height - h - y, bar_w, h
                    bar.attr
                        stroke: if coalition.seats > poll.min_seats then '#fff' else '#eee'
                        opacity: 0.98
                        fill: partyColors[party.name] || ('#ccc' && console.log(party.name))
                    sbc[party.name] = bar
                sbc.toplabel = label 'bar top center'
                l = coalition.key.replace(/,/g, '<br/>')
                if not HESSEN then l = l.replace('CDU', 'CDU/CSU')
                sbc.bottomlabel = label 'bar bottom center', l

            sbc = stacked_bars[coalition.key]

            # position the stacked bars
            y = 0
            x = Math.round 20 + i * bar_w * 1.8
            if coalition.seats < poll.min_seats
                x += offset if not justParties
                if possible
                    $('.desc-impossible').css
                        right: width - x+10
                    $('.desc-possible').css
                        left: x - offset
                    possible = false

            sbc.bottomlabel.stop(true).animate
                left: x-40+bar_w*0.5
                opacity: 1
            , 500, 'expoInOut'

            # make labels visible again, in case they're hidden

            $.each coalition.parties, (j, party) ->
                bar = sbc[party.name]
                # animate bar heights and y position first
                h = yscale party.seats
                barprops =
                    y: height - h - y - 0.5
                    height: h
                    width: bar_w
                bar.animate barprops, 500, 'expoInOut', () ->
                    props = # animate stacks x position last
                    bar.animate
                        x: x + 0.5
                        opacity: 1
                    , 500, 'expoInOut'
                    # make bar visible again, it case it was hidden
                y += h

            sbc.toplabel.stop(true).animate
                top: height - y - 22
                left: x-40+bar_w*0.5
                opacity: 1
            , 500, 'expoInOut'


            tl = coalition.seats - poll.min_seats
            if tl < 0
                sbc.toplabel.addClass 'negative'
            else
                sbc.toplabel.removeClass 'negative'
            tl = '+' + tl if tl > 0
            tl = '±' + tl if tl == 0
            sbc.toplabel.set tl

            $('.label.bottom').removeClass 'winner'

            # setTimeout () ->
            #     if not justParties
            #         if winner[election.yr] and stacked_bars[winner[election.yr]]
            #             stacked_bars[winner[election.yr]].bottomlabel.addClass 'winner'
            # , 2200


        # hide previous coalitions
        $.each stacked_bars, (key, bars) ->
            found = false
            $.each coalitions, (i, coalition) ->
                if coalition.key == key
                    found = true
                    false
                true
            if not found
                $.each bars, (party, bar) ->
                    bar.animate
                        opacity: 0
                    , 300

        # update grid lines
        init_grid_line = (addlbl=true) ->
            line = canvas.path 'M0,'+(height-.5)+' L'+width+','+(height-0.5)
            line.toBack()
            if addlbl
                lbl = label 'grid left'
                line.data 'lbl', lbl
                lbl = label 'grid right'
                line.data 'lbl2', lbl
            line

        move_grid_line = (line, seats) ->
            animprops =
                transform: 't0,'+(0 - yscale(seats))
            line.animate animprops, '800', 'expoInOut'
            if line.data('lbl')?
                lbl = line.data 'lbl'
                lbl.set seats
                lbl.stop(true).animate
                    opacity: 1
                    top: height - yscale(seats) - 8
                lbl = line.data 'lbl2'
                lbl.set seats
                lbl.stop(true).animate
                    opacity: 1
                    top: height - yscale(seats) - 8
            return

        if not grid.min_seats?
            grid.min_seats = init_grid_line()
            grid.min_seats.data('lbl').addClass 'min-seats'
            grid.min_seats.data('lbl2').addClass 'min-seats'
            # initialize base line
            grid.bottom = init_grid_line(false)  #.transform 't0,1'

        grid.bottom.toFront()

        move_grid_line grid.min_seats, poll.min_seats

        # init and move grid lines
        ticks = if $('.vis-coalitions').data('url') != 'hessen.json' then [100,200,300] else [10,20,30,40,50]
        _.each ticks, (seats) ->
            if not grid[seats]?
                grid[seats] = init_grid_line()
                grid[seats].attr
                    'stroke-dasharray': '- '
            move_grid_line grid[seats], seats
            lineprops =
                opacity: if seats+5 < poll.min_seats then 1 else 0
            grid[seats].animate lineprops, 500
            grid[seats].data('lbl').animate lineprops
            grid[seats].data('lbl2').animate lineprops

        # init and resize background
        if not bg?
            bg = canvas.rect 0,height-1,width,1
            bg.attr
                fill: '#f5f5f5'
                stroke: false
            bg.toBack()
        bgprops =
            y: height - yscale(poll.min_seats)
            height: yscale(poll.min_seats)
            opacity: 1

        if justParties
            $('.desc-intro').show()
            $('.desc-impossible, .desc-possible, .label.left').hide()
            bg.animate
                opacity: 0
        else
            $('.desc-intro').hide()
            $('.desc-impossible, .desc-possible, .label.left').show()
            bg.animate bgprops, 800, 'expoInOut'

    if location.hash.length and location.hash != '#activate'
        progn = location.hash.substr(1).replace(/\//, '-')
    else
        #progn = $($('.prognosen a').get(0)).attr('href').substr(1)
        progn = 'elections-nds-amtl-2342'

    $('.prognosen a[href=#'+progn+']').addClass 'active'

    prevJSON = undefined
    lastPollIndex = -1

    update = () ->

        $.ajax
            url: 'assets/data/'+$('.vis-coalitions').data('url')
            dataType: 'json'
        .done (json) ->

            firstLoad = _.isUndefined prevJSON

            if !prevJSON || json.length != prevJSON.length
                prevJSON = polls = json

                current_poll = false

                cont = $ '.prognosen'
                cont.html('')

                _.each polls, (poll, i) ->
                    li = $('<li><div class="provider">'+poll.institute+'</div><div>'+poll.time+'</div></li>')
                    if not current_poll
                        current_poll = poll
                        li.addClass 'active'
                        lastPollIndex = 0

                    li.data 'poll', poll
                    li.data 'poll-index', i
                    li.appendTo cont
                        .click (evt) ->
                            tgt = $(evt.target).parent()
                            render tgt.data 'poll'
                            $('.prognosen li').removeClass 'active'
                            tgt.addClass 'active'
                            lastPollIndex = tgt.data 'poll-index'
                            setTimeout () ->
                                $('.help').fadeIn(300)
                            , 4000

                pollFromHash = (hash) ->
                    [prov, time] = hash.substr(1).split '-'
                    if polls[prov]
                        _.find polls, (p) -> p.time == time

                if firstLoad
                    if location.hash
                        poll = pollFromHash location.hash
                        if poll
                            current_poll = poll

                render current_poll
                blocked = false

            setTimeout update, 10000

    update()

    $(window).keydown (evt) ->
        if evt.keyCode == 37  # left
            if lastPollIndex > 0
                render prevJSON[lastPollIndex-1]
                lastPollIndex--
        else if evt.keyCode == 39  # right
            if lastPollIndex < prevJSON.length-1
                render prevJSON[lastPollIndex+1]
                lastPollIndex++
        $('.prognosen li').removeClass 'active'
        $($('.prognosen li').get(lastPollIndex)).addClass 'active'
        if evt.keyCode == 37 or evt.keyCode == 39
            evt.preventDefault()
            evt.stopPropagation()
            $par = $('.prognosen').parent()
            dx = 465 - ($('.prognosen .active').offset().left - $par.offset().left)

            sl = $par.scrollLeft()
            $par.scrollLeft(sl - dx)

`
$.extend(jQuery.easing, {
    expoIn: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    expoOut: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    expoInOut: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
});
`