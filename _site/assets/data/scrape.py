# encoding: utf-8

import json
import requests
from bs4 import BeautifulSoup

r = requests.get('http://www.wahlrecht.de/news/2013/bundestagswahl-2013.html')

soup = BeautifulSoup(r.text)

table = soup.find(id='prognosen-hochrechnungen-tabelle')

parties = 'CDU,SPD,FDP,LINKE,GRÜNE'.split(',')
td_offset = 10

out = []

for tr in table.findAll('tr'):
    if tr.get('id') is not None:
        inst, time = tr.get('id').split('_')
        if len(inst) == 3:
            ths = tr.findAll('th')
            tds = tr.findAll('td')
            poll = dict(time=ths[0].text, institute=ths[1].text, seats=dict())
            for i in range(len(parties)):
                party = parties[i]
                seats = tds[td_offset+i].text
                try:
                    seats = int(seats)
                except:
                    seats = 0
                poll['seats'][party] = seats
            out.append(poll)

out.reverse()

json.dump(out, open('hochrechnungen.json', 'w'))
