// Raphael Easing Formulas

Raphael.easing_formulas['expoInOut'] = function (n, time, beg, diff, dur) {
    dur = 1000;
    time = n*1000;
    beg = 0;
    diff = 1;
    if (time===0) return beg;
    if (time==dur) return beg+diff;
    if ((time/=dur/2) < 1) return diff/2 * Math.pow(2, 10 * (time - 1)) + beg;
    return diff/2 * (-Math.pow(2, -10 * --time) + 2) + beg;
};

Raphael.easing_formulas['expoIn'] = function (n, time, beg, diff, dur) {
    dur = 1000;
    time = n*1000;
    beg = 0;
    diff = 1;
    return (time==0) ? beg : diff * Math.pow(2, 10 * (time/dur - 1)) + beg;
};

Raphael.easing_formulas['expoOut'] = function (n, time, beg, diff, dur) {
    dur = 1000;
    time = n*1000;
    beg = 0;
    diff = 1;
    return (time==dur) ? beg+diff : diff * (-Math.pow(2, -10 * time/dur) + 1) + beg;
};
(function($) { 
  $.fn.swipeEvents = function() {
    return this.each(function() {
      
      var startX,
          startY,
          $this = $(this);
      
      $this.bind('touchstart', touchstart);
      
      function touchstart(event) {
        var touches = event.originalEvent.touches;
        if (touches && touches.length) {
          startX = touches[0].pageX;
          startY = touches[0].pageY;
          $this.bind('touchmove', touchmove);
        }
        event.preventDefault();
      }
      
      function touchmove(event) {
        var touches = event.originalEvent.touches;
        if (touches && touches.length) {
          var deltaX = startX - touches[0].pageX;
          var deltaY = startY - touches[0].pageY;
          
          if (deltaX >= 50) {
            $this.trigger("swipeLeft");
            event.preventDefault();
          }
          if (deltaX <= -50) {
            $this.trigger("swipeRight");
            event.preventDefault();
          }
          if (deltaY >= 50) {
            $this.trigger("swipeUp");
          }
          if (deltaY <= -50) {
            $this.trigger("swipeDown");
          }
          if (Math.abs(deltaX) >= 50 || Math.abs(deltaY) >= 50) {
            $this.unbind('touchmove', touchmove);
          }
        }
      }
      
    });
  };
})(jQuery);
(function() {
  var Common, _ref;

  Common = (_ref = window.Common) != null ? _ref : window.Common = {};

  Common.classes = {
    CDU: 'CDU',
    SPD: 'SPD',
    FDP: 'FDP',
    LPDS: 'LIN',
    GRÜNE: 'GRU',
    PIRATEN: 'PIR'
  };

  Common.partyColors = {
    CDU: '#222222',
    SPD: '#E2001A',
    GRÜNE: '#1FA12D',
    FDP: '#F3E241',
    LINKE: '#8B1C62'
  };

  Common.humanNames = {
    '01-03': 'Braunschweig<sup>1</sup>',
    '24-28': 'Hannover<sup>2</sup>'
  };

  Common.ElectionSelector = function(data, active, callback, yr) {
    var blocked, currentActive, elsel, turnleft, turnright, update;
    elsel = $('<div class="election-selector" />').appendTo('#container');
    currentActive = active;
    blocked = false;
    update = function(a, evt) {
      var r;
      if (blocked) {
        return;
      }
      blocked = true;
      setTimeout(function() {
        return blocked = false;
      }, 1000);
      r = callback(a, evt);
      if (r) {
        currentActive = a;
        $('a', elsel).removeClass('active');
        return $('a.i-' + currentActive, elsel).addClass('active');
      }
    };
    $.each(data, function(i, item) {
      var a, y;
      y = yr != null ? yr(item) : item;
      a = $('<a><span class="long">' + (y < 80 ? '20' : '19') + '</span>' + y + '</a>');
      a.addClass('i-' + i);
      a.css({
        'margin-right': 10,
        cursor: 'pointer'
      });
      a.data('index', i);
      a.click(function(evt) {
        active = $(evt.target).data('index');
        currentActive = active;
        update(active, evt);
        return null;
      });
      if (i === active) {
        a.addClass('active');
      }
      return elsel.append(a);
    });
    turnleft = function(evt) {
      if (currentActive > 0) {
        active = currentActive - 1;
        return update(active, evt);
      }
    };
    turnright = function(evt) {
      active = currentActive + 1;
      if ($('a.i-' + active).length > 0) {
        return update(active, evt);
      }
    };
    $(window).on('keydown', function(evt) {
      if (evt.keyCode === 37) {
        return turnleft();
      } else if (evt.keyCode === 39) {
        return turnright();
      }
    });
    return elsel;
  };

  Common.CityLabels = [
    {
      name: 'Bremen',
      lon: 8.84,
      lat: 53.11
    }, {
      name: 'Hamburg',
      lon: 10.2,
      lat: 53.57
    }, {
      name: 'H',
      lon: 9.76,
      lat: 52.38
    }, {
      name: 'OS',
      lon: 8.03,
      lat: 52.28
    }, {
      name: 'OL',
      lon: 8.2,
      lat: 53.15
    }, {
      name: 'BS',
      lon: 10.53,
      lat: 52.26
    }, {
      name: 'GÖ',
      lon: 9.94,
      lat: 51.53
    }, {
      name: 'WHV',
      lon: 8.10,
      lat: 53.6
    }
  ];

}).call(this);

(function() {

  $(function() {
    var HESSEN, bar_w, bg, canvas, current_poll, elections, get_coalitions, grid, height, lastPollIndex, longInstName, partyColors, prevJSON, progn, render, stacked_bars, update, width, winner, _coalitions, _cont, _lblcont;
    _coalitions = ["CDU,SPD", "CDU,FDP", "CDU,FDP,PIRATEN", "CDU,GRÜNE", "SPD,GRÜNE", "SPD,FDP", "SPD,LINKE", "CDU,FDP,GRÜNE", "SPD,FDP,GRÜNE", "SPD,LINKE,GRÜNE"];
    _cont = $('#canvas');
    _lblcont = $('.vis-coalitions');
    canvas = Raphael(_cont.get(0));
    bar_w = 30;
    elections = null;
    width = _cont.width();
    height = _cont.height() - 12;
    stacked_bars = {};
    current_poll = null;
    grid = {};
    bg = null;
    partyColors = Common.partyColors;
    winner = {
      '08': 'CDU,FDP',
      '03': 'CDU,FDP',
      '98': 'SPD',
      '94': 'SPD',
      '90': 'SPD,GRÜNE'
    };
    HESSEN = $('.vis-coalitions').data('url') === 'hessen.json';
    if (!HESSEN) {
      _coalitions.push("CDU,LINKE");
    }
    $.fn.set = function(txt) {
      return $('span', this).html(txt);
    };
    get_coalitions = function(poll, justParties) {
      var coalitions, min_seats, seats, sum_seats;
      seats = poll.seats;
      coalitions = [];
      sum_seats = _.reduce(seats, function(s, v) {
        return s + v;
      }, 0);
      min_seats = Math.ceil((sum_seats + 0.5) * 0.5);
      _.each(_coalitions, function(parties, i) {
        var coalition;
        coalition = {
          id: i,
          key: parties,
          parties: [],
          seats: 0
        };
        _.each(parties.split(','), function(party) {
          if (!coalition) {
            return;
          }
          if (seats[party]) {
            coalition.parties.push({
              name: party,
              seats: seats[party]
            });
            return coalition.seats += seats[party];
          } else {
            coalition = null;
            return false;
          }
        });
        if (coalition != null) {
          coalition.parties.sort(function(a, b) {
            return b.seats - a.seats;
          });
          return coalitions.push(coalition);
        }
      });
      coalitions = coalitions.filter(function(coalition) {
        var makessense;
        if (coalition.parties.length === 3) {
          makessense = true;
          _.each(coalitions, function(c) {
            if (c.parties.length === 2) {
              if (c.parties[0].name === coalition.parties[0].name) {
                if (c.parties[1].name === coalition.parties[1].name || c.parties[1].name === coalition.parties[2].name) {
                  if (c.seats >= poll.min_seats) {
                    makessense = false;
                    return false;
                  }
                }
              }
            }
          });
          return makessense;
        } else {
          return true;
        }
      });
      _.each(seats, function(s, party) {
        if (s > min_seats * 0.5 || (justParties && s > 0)) {
          return coalitions.push({
            key: party,
            seats: s,
            parties: [
              {
                name: party,
                seats: s
              }
            ]
          });
        }
      });
      coalitions.sort(function(a, b) {
        if (a.seats >= min_seats && b.seats >= min_seats) {
          if (a.parties[0].seats !== b.parties[0].seats) {
            return b.parties[0].seats - a.parties[0].seats;
          }
        }
        return b.seats - a.seats;
      });
      poll.min_seats = min_seats;
      return coalitions;
    };
    longInstName = {
      ARD: 'ARD / Infratest dimap',
      ZDF: 'ZDF / Forschungsgr. Wahlen',
      RTL: 'RTL / Forsa',
      Wahlrecht: 'Wahlrecht.de',
      'Vorläufiges<br>Endergebnis': 'Vorläufiges Endergebnis'
    };
    render = function(poll, justParties, firstLoad) {
      var $dp, END, bgprops, coalitions, init_grid_line, label, move_grid_line, offset, possible, ticks, yscale;
      if (justParties == null) {
        justParties = false;
      }
      if (firstLoad == null) {
        firstLoad = false;
      }
      END = poll.time === '';
      $dp = $('.desc-possible');
      if (END && $dp.html() !== $dp.data('end')) {
        $dp.data('hochrechnung', $dp.html());
        $dp.html($dp.data('end'));
      } else if (!END && $dp.data('hochrechnung')) {
        $dp.html($dp.data('hochrechnung'));
      }
      if (END) {
        $('.info').addClass('end');
      } else {
        $('.info').removeClass('end');
      }
      $('.info').html('' + longInstName[poll.institute] + (poll.time ? ', Stand: ' + poll.time : ''));
      coalitions = get_coalitions(poll, justParties);
      bar_w = justParties ? 60 : 30;
      yscale = function(seats) {
        return Math.round(seats / coalitions[0].seats * height);
      };
      label = function(clss, txt) {
        var lbl;
        lbl = $('<div class="label ' + clss + '"><span /></div>');
        _lblcont.append(lbl);
        lbl.css({
          opacity: 0
        });
        if (txt != null) {
          lbl.set(txt);
        }
        return lbl;
      };
      $('.desc').hide();
      setTimeout(function() {
        return $('.desc').fadeIn(400);
      }, 1000);
      bar_w = Math.round((width - 220) / coalitions.length / 1.8);
      offset = width - coalitions.length * bar_w * 1.8 - 15;
      if (justParties) {
        bar_w = (width - 340) / coalitions.length / 1.8;
      }
      possible = true;
      $.each(coalitions, function(i, coalition) {
        var l, sbc, tl, x, y;
        if (!(stacked_bars[coalition.key] != null)) {
          sbc = stacked_bars[coalition.key] = {};
          $.each(coalition.parties, function(j, party) {
            var bar, h, x, y;
            x = 20 + i * bar_w * 1.8;
            if (justParties) {
              x += 340;
            }
            y = 0;
            h = 0;
            bar = canvas.rect(x, height - h - y, bar_w, h);
            bar.attr({
              stroke: coalition.seats > poll.min_seats ? '#fff' : '#eee',
              opacity: 0.98,
              fill: partyColors[party.name] || ('#ccc' && console.log(party.name))
            });
            return sbc[party.name] = bar;
          });
          sbc.toplabel = label('bar top center');
          l = coalition.key.replace(/,/g, '<br/>');
          if (!HESSEN) {
            l = l.replace('CDU', 'CDU/CSU');
          }
          sbc.bottomlabel = label('bar bottom center', l);
        }
        sbc = stacked_bars[coalition.key];
        y = 0;
        x = Math.round(20 + i * bar_w * 1.8);
        if (coalition.seats < poll.min_seats) {
          if (!justParties) {
            x += offset;
          }
          if (possible) {
            $('.desc-impossible').css({
              right: width - x + 10
            });
            $('.desc-possible').css({
              left: x - offset
            });
            possible = false;
          }
        }
        sbc.bottomlabel.stop(true).animate({
          left: x - 40 + bar_w * 0.5,
          opacity: 1
        }, 500, 'expoInOut');
        $.each(coalition.parties, function(j, party) {
          var bar, barprops, h;
          bar = sbc[party.name];
          h = yscale(party.seats);
          barprops = {
            y: height - h - y - 0.5,
            height: h,
            width: bar_w
          };
          bar.animate(barprops, 500, 'expoInOut', function() {
            var props;
            return props = bar.animate({
              x: x + 0.5,
              opacity: 1
            }, 500, 'expoInOut');
          });
          return y += h;
        });
        sbc.toplabel.stop(true).animate({
          top: height - y - 22,
          left: x - 40 + bar_w * 0.5,
          opacity: 1
        }, 500, 'expoInOut');
        tl = coalition.seats - poll.min_seats;
        if (tl < 0) {
          sbc.toplabel.addClass('negative');
        } else {
          sbc.toplabel.removeClass('negative');
        }
        if (tl > 0) {
          tl = '+' + tl;
        }
        if (tl === 0) {
          tl = '±' + tl;
        }
        sbc.toplabel.set(tl);
        return $('.label.bottom').removeClass('winner');
      });
      $.each(stacked_bars, function(key, bars) {
        var found;
        found = false;
        $.each(coalitions, function(i, coalition) {
          if (coalition.key === key) {
            found = true;
            false;
          }
          return true;
        });
        if (!found) {
          return $.each(bars, function(party, bar) {
            return bar.animate({
              opacity: 0
            }, 300);
          });
        }
      });
      init_grid_line = function(addlbl) {
        var lbl, line;
        if (addlbl == null) {
          addlbl = true;
        }
        line = canvas.path('M0,' + (height - .5) + ' L' + width + ',' + (height - 0.5));
        line.toBack();
        if (addlbl) {
          lbl = label('grid left');
          line.data('lbl', lbl);
          lbl = label('grid right');
          line.data('lbl2', lbl);
        }
        return line;
      };
      move_grid_line = function(line, seats) {
        var animprops, lbl;
        animprops = {
          transform: 't0,' + (0 - yscale(seats))
        };
        line.animate(animprops, '800', 'expoInOut');
        if (line.data('lbl') != null) {
          lbl = line.data('lbl');
          lbl.set(seats);
          lbl.stop(true).animate({
            opacity: 1,
            top: height - yscale(seats) - 8
          });
          lbl = line.data('lbl2');
          lbl.set(seats);
          lbl.stop(true).animate({
            opacity: 1,
            top: height - yscale(seats) - 8
          });
        }
      };
      if (!(grid.min_seats != null)) {
        grid.min_seats = init_grid_line();
        grid.min_seats.data('lbl').addClass('min-seats');
        grid.min_seats.data('lbl2').addClass('min-seats');
        grid.bottom = init_grid_line(false);
      }
      grid.bottom.toFront();
      move_grid_line(grid.min_seats, poll.min_seats);
      ticks = $('.vis-coalitions').data('url') !== 'hessen.json' ? [100, 200, 300] : [10, 20, 30, 40, 50];
      _.each(ticks, function(seats) {
        var lineprops;
        if (!(grid[seats] != null)) {
          grid[seats] = init_grid_line();
          grid[seats].attr({
            'stroke-dasharray': '- '
          });
        }
        move_grid_line(grid[seats], seats);
        lineprops = {
          opacity: seats + 5 < poll.min_seats ? 1 : 0
        };
        grid[seats].animate(lineprops, 500);
        grid[seats].data('lbl').animate(lineprops);
        return grid[seats].data('lbl2').animate(lineprops);
      });
      if (!(bg != null)) {
        bg = canvas.rect(0, height - 1, width, 1);
        bg.attr({
          fill: '#f5f5f5',
          stroke: false
        });
        bg.toBack();
      }
      bgprops = {
        y: height - yscale(poll.min_seats),
        height: yscale(poll.min_seats),
        opacity: 1
      };
      if (justParties) {
        $('.desc-intro').show();
        $('.desc-impossible, .desc-possible, .label.left').hide();
        return bg.animate({
          opacity: 0
        });
      } else {
        $('.desc-intro').hide();
        $('.desc-impossible, .desc-possible, .label.left').show();
        return bg.animate(bgprops, 800, 'expoInOut');
      }
    };
    if (location.hash.length && location.hash !== '#activate') {
      progn = location.hash.substr(1).replace(/\//, '-');
    } else {
      progn = 'elections-nds-amtl-2342';
    }
    $('.prognosen a[href=#' + progn + ']').addClass('active');
    prevJSON = void 0;
    lastPollIndex = -1;
    update = function() {
      return $.ajax({
        url: 'assets/data/' + $('.vis-coalitions').data('url'),
        dataType: 'json'
      }).done(function(json) {
        var blocked, cont, firstLoad, poll, pollFromHash, polls;
        firstLoad = _.isUndefined(prevJSON);
        if (!prevJSON || json.length !== prevJSON.length) {
          prevJSON = polls = json;
          current_poll = false;
          cont = $('.prognosen');
          cont.html('');
          _.each(polls, function(poll, i) {
            var li;
            li = $('<li><div class="provider">' + poll.institute + '</div><div>' + poll.time + '</div></li>');
            if (!current_poll) {
              current_poll = poll;
              li.addClass('active');
              lastPollIndex = 0;
            }
            li.data('poll', poll);
            li.data('poll-index', i);
            return li.appendTo(cont.click(function(evt) {
              var tgt;
              tgt = $(evt.target).parent();
              render(tgt.data('poll'));
              $('.prognosen li').removeClass('active');
              tgt.addClass('active');
              lastPollIndex = tgt.data('poll-index');
              return setTimeout(function() {
                return $('.help').fadeIn(300);
              }, 4000);
            }));
          });
          pollFromHash = function(hash) {
            var prov, time, _ref;
            _ref = hash.substr(1).split('-'), prov = _ref[0], time = _ref[1];
            if (polls[prov]) {
              return _.find(polls, function(p) {
                return p.time === time;
              });
            }
          };
          if (firstLoad) {
            if (location.hash) {
              poll = pollFromHash(location.hash);
              if (poll) {
                current_poll = poll;
              }
            }
          }
          render(current_poll);
          blocked = false;
        }
        return setTimeout(update, 10000);
      });
    };
    update();
    return $(window).keydown(function(evt) {
      var $par, dx, sl;
      if (evt.keyCode === 37) {
        if (lastPollIndex > 0) {
          render(prevJSON[lastPollIndex - 1]);
          lastPollIndex--;
        }
      } else if (evt.keyCode === 39) {
        if (lastPollIndex < prevJSON.length - 1) {
          render(prevJSON[lastPollIndex + 1]);
          lastPollIndex++;
        }
      }
      $('.prognosen li').removeClass('active');
      $($('.prognosen li').get(lastPollIndex)).addClass('active');
      if (evt.keyCode === 37 || evt.keyCode === 39) {
        evt.preventDefault();
        evt.stopPropagation();
        $par = $('.prognosen').parent();
        dx = 465 - ($('.prognosen .active').offset().left - $par.offset().left);
        sl = $par.scrollLeft();
        return $par.scrollLeft(sl - dx);
      }
    });
  });

  
$.extend(jQuery.easing, {
    expoIn: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    expoOut: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    expoInOut: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
});
;


}).call(this);
